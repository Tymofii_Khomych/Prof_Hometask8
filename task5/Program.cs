﻿using System;
using Newtonsoft.Json;

public class User
{
    public string Name { get; set; }
    public int Age { get; set; }
    public string Email { get; set; }

    public User(string name, int age, string email)
    {
        Name = name;
        Age = age;
        Email = email;
    }

    public override string ToString()
    {
        return $"User: Name={Name}, Age={Age}, Email={Email}";
    }
}

class Program
{
    static void Main(string[] args)
    {
        User user = new User("John", 30, "john@example.com");

        string json = JsonConvert.SerializeObject(user, Formatting.Indented);

        Console.WriteLine("Serialized JSON:");
        Console.WriteLine(json);

        // Десеріалізація JSON-рядка у об'єкт
        User deserializedUser = JsonConvert.DeserializeObject<User>(json);

        Console.WriteLine("Deserialized User:");
        Console.WriteLine(deserializedUser);
    }
}