﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using task2;
namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {

            try
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Person));
                FileStream stream = new FileStream("Serialize.xml", FileMode.Open, FileAccess.Read, FileShare.Read);
                Person person = null;

                person = xmlSerializer.Deserialize(stream) as Person;

                Console.WriteLine($"Name: {person.Name}");
                Console.WriteLine($"Age: {person.Age}");
                Console.WriteLine(person.Working ? "Still working" : "Resting");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
