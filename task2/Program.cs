﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Person));
            FileStream file = new FileStream("Serialize.xml", FileMode.Create, FileAccess.Write, FileShare.Read);
            Person person = new Person();

            xmlSerializer.Serialize(file, person);
            file.Close();
        }
    }
}
