﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    public class Person
    {
        private string name = "Tony";
        private int age = 19;
        private bool working = true;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public bool Working
        {
            get { return working; }
            set { working = value; }
        }
    }
}
